package de.kotlincook.chess

import de.kotlincook.chess.Color.*


enum class Color {
    BLACK, WHITE
}

enum class HorCoord {
    A, B, C, D, E, F, G, H;
    companion object {
        fun iterable() = values().asIterable()
        operator fun iterator() = iterable().iterator()
    }
}

enum class VertCoord {
    _1, _2, _3, _4, _5, _6, _7, _8;
    companion object {
        fun iterable() = values().asIterable()
        operator fun iterator() = iterable().iterator()
    }
}

data class ChessCoord(val hor: HorCoord, val vert: VertCoord) {
    companion object {
        fun iterable(): Iterable<ChessCoord> {
            return VertCoord.iterable().reversed().flatMap {
                vert -> HorCoord.iterable().map { hor -> ChessCoord(hor, vert) }
            }
        }
        operator fun iterator() = iterable().iterator()
    }

    fun color() = if ((vert.ordinal + hor.ordinal).isEven()) BLACK else WHITE

    override fun toString() = "$hor$vert"
}

data class ChessMove(val from: ChessCoord, val to: ChessCoord)

fun Int.isEven() = this % 2 == 0 // optional

//fun main(args: Array<String>) {
//    val str = "asdfbasdbfb"
//    str.substring(1, 20)
//    for ((hor, vert) in ChessCoord) {
//        println("($hor, $vert)")
//    }
//}

