package de.kotlincook.chess.ui

import com.vaadin.flow.component.ComponentEvent
import com.vaadin.flow.component.DomEvent
import com.vaadin.flow.component.UI
import com.vaadin.flow.component.html.Div
import de.kotlincook.chess.ChessCoord
import de.kotlincook.chess.ChessMove
import de.kotlincook.chess.Color.BLACK
import de.kotlincook.chess.Color.WHITE
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

@DomEvent("drop")
class DropEvent(source: Square, fromClient: Boolean) : ComponentEvent<Square>(source, fromClient)

class Square(override val coord: ChessCoord) : Div(), Locatable {

    override val board
        get() = if (parent.isPresent) parent.get() as Board
        else throw IllegalStateException("$this does not have a parent")


    var piece: Piece? by object : ReadWriteProperty<Square, Piece?> {

            override fun getValue(thisRef: Square, property: KProperty<*>): Piece? {
                val optPiece = children.findAny()
                return if (optPiece.isPresent) optPiece.get() as Piece else null
            }

            override fun setValue(thisRef: Square, property: KProperty<*>, value: Piece?) {
                removeAll()
                if (value != null) add(value)
            }
        }

    //    var piece: Piece? by Delegate
    //
    //    object Delegate : ReadWriteProperty<Square, Piece?> {
    //        override fun getValue(thisRef: Square, property: KProperty<*>): Piece? {
    //            val optPiece = Square::getChildren.call().findAny()
    //            return if (optPiece.isPresent) optPiece.get() as Piece else null
    //        }
    //
    //        override fun setValue(thisRef: Square, property: KProperty<*>, value: Piece?) {
    //            Square::removeAll.call()
    //            if (value != null) Square::add.call(value)
    //        }
    //    }

    init {
        setId("square_$coord")
        className = when (coord.color()) {
            BLACK -> "square black"
            WHITE -> "square white"
        }

        // Drag & Drop:
        //
        UI.getCurrent().page.addDropSupport(element)
        addListener(DropEvent::class.java) { e ->
            val from = board.moveFrom
            if (from == null) {
                throw IllegalStateException("There is no source of dragged object")
            }
            this.board.performMove(ChessMove(from.coord, e.source.coord))
        }
    }

}