package de.kotlincook.chess.ui

import com.vaadin.flow.component.dependency.StyleSheet
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.page.Push
import com.vaadin.flow.router.Route
import de.kotlincook.chess.ChessCoord
import de.kotlincook.chess.ChessMove
import de.kotlincook.chess.HorCoord
import de.kotlincook.chess.HorCoord.*
import de.kotlincook.chess.VertCoord
import de.kotlincook.chess.VertCoord.*
import reactor.core.publisher.Flux
import reactor.core.publisher.UnicastProcessor

@Push
@Route("chess")
@StyleSheet("frontend://chess.css")
class Board(private val chessMovePublisher: UnicastProcessor<ChessMove>,
            chessMoves: Flux<ChessMove>): Div() {

    private val squares = object  {
        init {
            for (coord in ChessCoord) {
                add(Square(coord))
            }
        }
        operator fun get(hor: HorCoord, vert: VertCoord) = at(ChessCoord(hor, vert))

        fun at(coord: ChessCoord) =
                try {
                    val optComp = children.filter { it is Square && it.coord == coord }.findAny()
                    optComp.get() as Square
                } catch (e: Exception) {
                    throw IllegalStateException("There is no Square at $coord}")
                }
    }

    var moveFrom: Piece? = null

    init {
        setId("board")
        className = "board"

        squares[A,_1].piece = Piece("frontend/icons/rookWhite.png", "white_rook_A")
        squares[B,_1].piece = Piece("frontend/icons/knightWhite1.png", "white_knight_B")
        squares[C,_1].piece = Piece("frontend/icons/bishopWhite.png", "white_bishop_C")
        squares[D,_1].piece = Piece("frontend/icons/queenWhite.png", "white_queen_D")
        squares[E,_1].piece = Piece("frontend/icons/kingWhite.png", "white_king_E")
        squares[F,_1].piece = Piece("frontend/icons/bishopWhite.png", "white_bishop_F")
        squares[G,_1].piece = Piece("frontend/icons/knightWhite2.png", "white_knight_G")
        squares[H,_1].piece = Piece("frontend/icons/rookWhite.png", "white_rook_H")
        for (horCoord in HorCoord.values()) {
            squares[horCoord,_2].piece = Piece("frontend/icons/pawnWhite.png", "white_pawn_" + horCoord)
        }
        squares[A,_8].piece = Piece("frontend/icons/rookBlack.png", "black_rook_A")
        squares[B,_8].piece = Piece("frontend/icons/knightBlack1.png", "black_knight_B")
        squares[C,_8].piece = Piece("frontend/icons/bishopBlack.png", "black_bishop_C")
        squares[D,_8].piece = Piece("frontend/icons/queenBlack.png", "black_queen_D")
        squares[E,_8].piece = Piece("frontend/icons/kingBlack.png", "black_king_E")
        squares[F,_8].piece = Piece("frontend/icons/bishopBlack.png", "black_bishop_F")
        squares[G,_8].piece = Piece("frontend/icons/knightBlack2.png", "black_knight_G")
        squares[H,_8].piece = Piece("frontend/icons/rookBlack.png", "black_rook_H")
        for (horCoord in HorCoord.values()) {
            squares[horCoord,_7].piece = Piece("frontend/icons/pawnBlack.png", "black_pawn_" + horCoord)
        }
        chessMoves.subscribe { chessMove -> handleIncomingMove(chessMove) }
    }// initializer


    fun handleIncomingMove(chessMove: ChessMove) {
        ui.ifPresent { ui ->
            ui.access {
                with(chessMove) {
                    val pieceFrom = squares.at(from).piece
                    if (pieceFrom != null) {
                        squares.at(to).piece = pieceFrom
                    }
                }
                println("$chessMove.from -> $chessMove.to")
            }
        }
    }

    fun performMove(move: ChessMove) {
        chessMovePublisher.onNext(move)
    }

    fun squareAt(coord: ChessCoord) = squares.at(coord)

    fun pieceAt(coord: ChessCoord) = squareAt(coord).piece

}