package de.kotlincook.chess.ui

import com.vaadin.flow.component.*
import com.vaadin.flow.component.html.Image


@DomEvent("dragstart")
class DragstartEvent(source : Piece, fromClient : Boolean) : ComponentEvent<Piece>(source, fromClient)

class Piece(src: String, alt: String) : Image(src, alt), Locatable {

    override val board
        get() = if (parent.isPresent) (parent.get() as Square).board
                else throw IllegalStateException("$this does not have a parent")

    init {
        className = "piece"
        setId(alt)

        // Drag & Drop:
        //
        UI.getCurrent().page.addDragSupport(element)
        addListener(DragstartEvent::class.java) { e -> board.moveFrom = e.source }
    }

}