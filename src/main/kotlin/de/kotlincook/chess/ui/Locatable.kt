package de.kotlincook.chess.ui

import de.kotlincook.chess.ChessCoord

interface Locatable {

    val board: Board
    val coord: ChessCoord
        get() = ChessCoord.iterable().first {
            coord -> board.squareAt(coord) === this || board.pieceAt(coord) === this
        }
}