package de.kotlincook.chess

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import reactor.core.publisher.Flux
import reactor.core.publisher.UnicastProcessor

@SpringBootApplication
class Application : SpringBootServletInitializer() {

    @Bean
    fun chessMovePublisher(): UnicastProcessor<ChessMove> {
        return UnicastProcessor.create<ChessMove>()
    }

    @Bean
    fun chessMoves(messagePublisher: UnicastProcessor<ChessMove>): Flux<ChessMove> {
        return messagePublisher.replay(30).autoConnect()
    }
}

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
